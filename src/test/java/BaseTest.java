import Page.BasePage;
import Page.SearchPage;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.junit.ScreenShooter;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;

public class BaseTest {

    BasePage base = new BasePage();
    SearchPage searchPage = new SearchPage();

    private static Logger logger = LoggerFactory.getLogger(Logger.class);

    public static final String AUTORIZATION_URL = "https://new.internet-start.net/";


    //данное правило работает на упавшие тесты (делает скриншот)
    @Rule
    public ScreenShooter makeScreenshotOnFailure = ScreenShooter.failedTests();

    @BeforeClass
    public static void setUp(){
        Configuration.browser = "chrome";
        //Configuration.holdBrowserOpen = true;
        Configuration.startMaximized = true;
       //Configuration.assertionMode = AssertionMode.SOFT;
    }

    @Test
    public void iStartPageTest1(){
        open(AUTORIZATION_URL);
        logger.info("переход на главную страницу поиска");
        base.fillSearchInput("Selenide");
        logger.info("ввод в инпут поиска значения Selenide");
        base.clicToSearchButton();
        logger.info("нажать кнопку поиска");
        sleep(2000);
        searchPage.waitForLoadPreview();
        searchPage.checkFirstResult("selenide.org");
        logger.info("проверяем первый рузультат выдачи на момент вхождения текста \"selenide.org\"");
    }

    @Test()
    public void iStartPageTest2() {
        logger.info("переход на главную страницу поиска");
        open(AUTORIZATION_URL);
        logger.info("ввод в инпут поиска значения Selenide");
        base.fillSearchInput("driverpack");
        logger.info("выбор первого значения из выпадающего списка с подсказками");
        base.ClickToElementHelpBar(0);
        sleep(2000);
        searchPage.waitForLoadPreview();
        logger.info("проверяем первый рузультат выдачи на момент вхождения текста \"DriverPack Solution\"");
        searchPage.checkFirstResult("DriverPack Solution");
    }



}
