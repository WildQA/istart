package Page;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class BasePage {

    private SelenideElement BasePageSearchInput = $(By.xpath("//input[@id='customSearchInput']"));
    private SelenideElement Searchbuttn = $(By.xpath("//div[@id='searchForm']/div//button[@type='button']"));
    private SelenideElement searchBar = $(By.xpath("///ul[@role='listbox']"));

    private ElementsCollection el = $$(By.xpath("//ul[@role='listbox']/li"));


    public BasePage clicToSearchButton(){
        Searchbuttn.should(Condition.exist);
        Searchbuttn.click();
        return this;
    }

    public BasePage fillSearchInput(String s){
        BasePageSearchInput.shouldBe(Condition.visible);
        BasePageSearchInput.sendKeys(s);
        return this;
    }

    public BasePage ClickToElementHelpBar(Integer value){
        el.get(value).shouldBe(Condition.visible).click();
        return this;
    }




}
