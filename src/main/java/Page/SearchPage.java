package Page;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchPage {

    private ElementsCollection res = $$(By.xpath("//div[@class='gsc-control-wrapper-cse']//div[@class='gsc-results gsc-webResult']/div[1]/div[1]"));

    private SelenideElement preview = $(By.xpath("/html//div[@id='previews']"));

    public  SearchPage waitForLoadPreview(){
        preview.shouldBe(visible);
        return this;
    }

    public SearchPage checkFirstResult(String sometext){
        res.findBy(text(sometext)).shouldBe(visible);
        return this;
    }
}
